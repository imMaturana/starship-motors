<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Adicionar</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
  <?php include_once 'layout/header.php'; ?>

  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <div class="row center">
        <div class="col s12 m12 l12 xl12">
          <h3 class="light">Adicionar carro</h3>
          <form action="controllers/create.php" method="post">
            <div class="input-field col s12 m6">
              <input type="text" name="marca" id="marca">
              <label for="marca">Marca</label>
            </div>
            <div class="input-field col s12 m6">
              <input type="text" name="modelo" id="modelo">
              <label for="modelo">Modelo</label>
            </div>
            <div class="input-field col s12 m3">
              <input type="text" name="descricao" id="descricao">
              <label for="descricao">Descrição</label>
            </div>
            <div class="input-field col s12 m3">
              <input type="text" name="ano" id="ano">
              <label for="ano">Ano</label>
            </div>
            <div class="input-field col s12 m3">
              <input type="text" name="cor" id="cor">
              <label for="cor">Cor</label>
            </div>
            <div class="input-field col s12 m3">
              <input type="text" name="placa" id="placa">
              <label for="placa">Placa</label>
            </div>
            <div class="input-field col s12 m12">
              <input type="number" name="valor" id="valor">
              <label for="valor">Valor</label>
            </div>
            <button type="submit" name="adicionar" class="btn">
              Adicionar
            </button>
          </form>
        </div>
      </div>
      <br><br>

    </div>
  </div>

  <?php include_once 'layout/footer.php'; ?>

  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>

  </body>
</html>
