<?php

$host = "localhost";
$port = 3306;
$user = "root";
$password = "root";
$database = "starship";

$conn = mysqli_connect($host, $user, $password, $database, $port);

if (!$conn) {
  die("Erro de conexão: " . mysqli_connect_error());
}

?>