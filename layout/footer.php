<footer class="page-footer purple">
  <div class="container">
    <div class="row">
      <div class="col l12 s12">
        <p class="grey-text text-lighten-4">
          Atividade desenvolvida por <a href="https://codeberg.org/maturana">Gabriel Maturana Teixeira</a> no <span class="ifro">IFRO - Campus Ji-Paraná</span> na matéria de Desenvolvimento Web, mestrada pelo professor <a href="https://github.com/maigonp" target="blank">Maigon Nacib Pontuschka</a> do Curso Técnico em Informática.
        </p>
      </div>
  </div>
  <div class="footer-copyright">
    <div class="container center">
    Made by <a class="orange-text text-lighten-3" href="http://materializecss.com">Materialize</a>
    </div>
  </div>
</footer>